#!/bin/sh

usage() {
	echo "time.sh - a script to measure interesting things from min2's log file" 1>&2
	echo "Usage: time.sh file_name what" 1>&2
	echo "  where \"what\" is:" 1>&2
	echo "    download                time of file download(s)" 1>&2
	echo "    get_responsible_node    time of responsible node searches" 1>&2
}

time_to_sec() {
	if [ ! $# -eq 1 ]; then
                echo "error: time_to_sec needs 1 argument" 1>&2
                exit 1
        fi

	date -u -d "$1" +"%s.%N"
}

time_diff() {
	if [ $# -lt 2 ]; then
		echo "error: time_diff needs >=2 arguments" 1>&2
		exit 1;
	fi

	from_str="$1"
	to_str="$2"
	text="$3"

	from_sec=$(time_to_sec "$from_str")
	to_sec=$(time_to_sec "$to_str")

	diff=$(date -u -d "1970-01-01 $to_sec sec - $from_sec sec" +"%s.%N")

	case "$diff" in
	-*)
		echo "error: negative time difference ($diff) between '$from_str' and '$to_str' (text: '$text')" 1>&2
		exit 1
		;;
	*)
		echo "$diff" "$text"
		;;
	esac
}

get_times() {
	if [ $# -lt 3 ]; then
                echo "error: get_times needs >=3 arguments" 1>&2
                exit 1;
        fi

	file="$1"
	start_str="$2"
	end_str="$3"
	start_sed="$4"
	end_sed="$5"

	grep "$start_str" "$file" | sed "$start_sed" | awk '{ print NR " " $0 }' | sort -k4 > starts.tmp
	grep "$end_str" "$file" | sed "$end_sed" | sort -k3 > ends.tmp

	starts_count=$(wc -l < starts.tmp)
	ends_count=$(wc -l < ends.tmp)

	if [ ! "0$starts_count" -eq "0$ends_count" ]; then
		echo "error: cannot match $starts_count starts with $ends_count ends" 1>&2
                exit 1
	fi

	join -1 4 -2 3 starts.tmp ends.tmp | sort -k2n | cut -d' ' -f1,3- > diff.tmp

	while read id from_1 from_2 to_1 to_2; do
		time_diff "$from_1 $from_2" "$to_1 $to_2" "$id"
	done < diff.tmp
}

#get_times log_0.log 'download start: starting download [0-9a-f]\{40\}' 'dht operator(): get([0-9a-f]\{40\}) - on_download_finished' 's, 0x.*\([0-9a-f]\{40\}\).*, \1,' 's, 0x.*\([0-9a-f]\{40\}\).*, \1,'
#grep -e 'download start: starting download [0-9a-f]\{40\}' log_0.log | sed 's, 0x.*\([0-9a-f]\{40\}\), \1,'
#time_diff "2016-06-13 18:03:05.886811" "2016-06-13 18:04:05.886812"
#time_diff "2016-06-13 18:04:05.886812" "2016-06-13 18:03:05.886811"

if [ $# -lt 2 ]; then
	usage
	exit 1
fi

file="$1"
what="$2"

case "$what" in
	download)
		get_times "$file" \
			'download start: starting download [0-9a-f]\{40\}' \
			'dht operator(): get([0-9a-f]\{40\}) - on_download_finished' \
			's, 0x.*\([0-9a-f]\{40\}\).*, \1,' \
			's, 0x.*\([0-9a-f]\{40\}\).*, \1,' \
	;;
	get_responsible_node)
		get_times "$file" \
			'main operator(): getting node info for responsible node for [0-9a-f]\{40\}' \
			'main operator(): found node info for responsible node for [0-9a-f]\{40\}: [0-9a-f]\{40\}' \
			's, 0x.*\([0-9a-f]\{40\}\).*, \1,' \
			's, 0x.*\([0-9a-f]\{40\}\): .*, \1,' \
	;;
	*)
		usage
		exit 1;
	;;
esac
